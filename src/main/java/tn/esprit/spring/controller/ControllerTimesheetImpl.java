package tn.esprit.spring.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.services.IEmployeService;
import tn.esprit.spring.services.IEntrepriseService;
import tn.esprit.spring.services.ITimesheetService;

import java.util.Date;
import java.util.List;

@Controller
public class ControllerTimesheetImpl {
    private static final Logger LOG = LogManager.getLogger(ControllerTimesheetImpl.class);

    @Autowired
    IEmployeService iemployeservice;
    @Autowired
    IEntrepriseService ientrepriseservice;
    @Autowired
    ITimesheetService itimesheetservice;

    public int ajouterMission(Mission mission) {
        LOG.info("Method ajouterMission ");

        itimesheetservice.ajouterMission(mission);
        return mission.getId();
    }

    public void affecterMissionADepartement(int missionId, int depId) {
        LOG.info("Method affecterMissionADepartement ");
        itimesheetservice.affecterMissionADepartement(missionId, depId);

    }

    public void ajouterTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin) {
        LOG.info("Method ajouterTimesheet ");
        itimesheetservice.ajouterTimesheet(missionId, employeId, dateDebut, dateFin);

    }


    public void validerTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin, int validateurId) {
        LOG.info("Method validerTimesheet ");
        itimesheetservice.validerTimesheet(missionId, employeId, dateDebut, dateFin, validateurId);

    }

    public List<Mission> findAllMissionByEmployeJPQL(int employeId) {
        LOG.info("Method findAllMissionByEmployeJPQL ");

        return itimesheetservice.findAllMissionByEmployeJPQL(employeId);
    }


    public List<Employe> getAllEmployeByMission(int missionId) {
        LOG.info("Method getAllEmployeByMission ");

        return itimesheetservice.getAllEmployeByMission(missionId);
    }
}
