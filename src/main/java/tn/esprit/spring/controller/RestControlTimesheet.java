package tn.esprit.spring.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.MissionDTO;
import tn.esprit.spring.services.ITimesheetService;

import java.util.Date;
import java.util.List;

@RestController
public class RestControlTimesheet {
    private static final Logger LOG = LogManager.getLogger(RestControlTimesheet.class);

    @Autowired
    ITimesheetService itimesheetservice;


    @PostMapping("/ajouterMission")
    @ResponseBody
    public int ajouterMission(@RequestBody MissionDTO mission) {
        LOG.info("Start Method ajouterMission");
        itimesheetservice.ajouterMission(mission.getMission());
        LOG.info("End Method ajouterMission");
        return mission.getId();

    }

    @PutMapping(value = "/affecterMissionADepartement/{idmission}/{iddept}")
    public void affecterMissionADepartement(@PathVariable("idmission") int missionId, @PathVariable("iddept") int depId) {
        LOG.info("Start Method affecterMissionADepartement");
        itimesheetservice.affecterMissionADepartement(missionId, depId);
        LOG.info("End Method affecterMissionADepartement");

    }


    @PostMapping("/ajouterTimesheet/idmission/idemp/dated/datef")
    @ResponseBody
    public void ajouterTimesheet(@PathVariable("idmission") int missionId, @PathVariable("idemp") int employeId, @PathVariable("dated") Date dateDebut, @PathVariable("datef") Date dateFin) {
        LOG.info("Start Method ajouterTimesheet");
        itimesheetservice.ajouterTimesheet(missionId, employeId, dateDebut, dateFin);
        LOG.info("End Method ajouterTimesheet");

    }

    @PutMapping(value = "/validerTimesheet/{idmission}/{idemp}/{dated}/{datef}/{idval}")
    public void validerTimesheet(@PathVariable("idmission") int missionId, @PathVariable("idemp") int employeId, @PathVariable("dated") Date dateDebut, @PathVariable("datef") Date dateFin, @PathVariable("idval") int validateurId) {
        LOG.info("Start Method validerTimesheet");
        itimesheetservice.validerTimesheet(missionId, employeId, dateDebut, dateFin, validateurId);
        LOG.info("End Method validerTimesheet");

    }

    @GetMapping(value = "findAllMissionByEmployeJPQL/{idemp}")
    @ResponseBody
    public List<Mission> findAllMissionByEmployeJPQL(@PathVariable("idemp") int employeId) {
        LOG.info("Start Method findAllMissionByEmployeJPQL");
        List<Mission> list = itimesheetservice.findAllMissionByEmployeJPQL(employeId);
        LOG.info("End Method findAllMissionByEmployeJPQL");
        return list;

    }

    @GetMapping(value = "getAllEmployeByMission/{idmission}")
    @ResponseBody
    public List<Employe> getAllEmployeByMission(@PathVariable("idmission") int missionId) {
        LOG.info("Start Method getAllEmployeByMission");
        List<Employe> list = itimesheetservice.getAllEmployeByMission(missionId);
        LOG.info("End Method getAllEmployeByMission");
        return list;
    }
}
