package tn.esprit.spring.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.spring.entities.*;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.MissionRepository;
import tn.esprit.spring.repository.TimesheetRepository;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TimesheetServiceImpl implements ITimesheetService {
    private static final Logger LOG = LogManager.getLogger(TimesheetServiceImpl.class);

    @Autowired
    MissionRepository missionRepository;
    @Autowired
    DepartementRepository deptRepoistory;
    @Autowired
    TimesheetRepository timesheetRepository;
    @Autowired
    EmployeRepository employeRepository;

    public int ajouterMission(Mission mission) {
        LOG.info(MessageFormat.format("Mission: {0}", mission));
        missionRepository.save(mission);
        LOG.debug(MessageFormat.format("Saved Mission: {0}", mission));
        LOG.debug(MessageFormat.format("Mission ID: {0}", mission.getId()));

        return mission.getId();
    }

    public void affecterMissionADepartement(int missionId, int depId) {
        LOG.info(MessageFormat.format("Mission ID: {0}", missionId));
        LOG.info(MessageFormat.format("Departement ID: {0}", depId));
        Mission mission = null;
        Departement dep = null;
        Optional<Mission> missionR = missionRepository.findById(missionId);
        Optional<Departement> deptR = deptRepoistory.findById(depId);
        if (missionR.isPresent()) {
            mission = missionR.get();
            LOG.debug(MessageFormat.format("Fetched Mission: {0}", missionId));
        }
        if (deptR.isPresent()) {
            dep = deptR.get();
            LOG.debug(MessageFormat.format("Fetched Departement: {0}", depId));
        }
        assert mission != null;
        mission.setDepartement(dep);
        LOG.info(MessageFormat.format("Add new Departement To Mission: {0}", mission));

        missionRepository.save(mission);
        LOG.info(MessageFormat.format("Saved Mission: {0}", mission));
    }

    public void ajouterTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin) {
        TimesheetPK timesheetPK = new TimesheetPK();
        timesheetPK.setDateDebut(dateDebut);
        timesheetPK.setDateFin(dateFin);
        timesheetPK.setIdEmploye(employeId);
        timesheetPK.setIdMission(missionId);
        LOG.info(MessageFormat.format("Timesheet Primary Key: {0}", timesheetPK));
        Timesheet timesheet = new Timesheet();
        LOG.debug(MessageFormat.format("New Timesheet: {0}", timesheet));

        timesheet.setTimesheetPK(timesheetPK);
        timesheet.setValide(false); //par defaut non valide
        timesheetRepository.save(timesheet);
        LOG.info(MessageFormat.format("Saved Timesheet: {0}", timesheet));

    }


    public void validerTimesheet(int missionId, int employeId, Date dateDebut, Date dateFin, int validateurId) {
        LOG.info("In valider Timesheet");
        Employe validateur = null;
        Mission mission = null;
        Optional<Employe> employeR = employeRepository.findById(validateurId);
        Optional<Mission> missionR = missionRepository.findById(missionId);
        if (employeR.isPresent() && missionR.isPresent()) {
            validateur = employeR.get();
            mission = missionR.get();
        }
        //verifier s'il est un chef de departement (interet des enum)
        assert validateur != null;
        if (!validateur.getRole().equals(Role.CHEF_DEPARTEMENT)) {
            LOG.debug(MessageFormat.format("l''employe doit etre chef de departement pour valider une feuille de temps !{0}", validateur));
            return;
        }
        //verifier s'il est le chef de departement de la mission en question
        boolean chefDeLaMission = false;
        for (Departement dep : validateur.getDepartements()) {
            if (dep.getId() == mission.getDepartement().getId()) {
                chefDeLaMission = true;
                break;
            }
        }
        if (!chefDeLaMission) {
            LOG.debug(MessageFormat.format("l''employe doit etre chef de departement de la mission en question{0}", false));
            return;
        }
//
        TimesheetPK timesheetPK = new TimesheetPK(missionId, employeId, dateDebut, dateFin);
        Timesheet timesheet = timesheetRepository.findBytimesheetPK(timesheetPK);
        LOG.info(MessageFormat.format("Fetched Timesheet {0}", timesheet));
        timesheet.setValide(true);
        LOG.info(MessageFormat.format("Updated Timesheet {0}", timesheet));

        //Comment Lire une date de la base de données  ☺ ???????????????☺
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        LOG.debug(MessageFormat.format("dateDebut   : {0}", dateFormat.format(timesheet.getTimesheetPK().getDateDebut())));
        timesheetRepository.save(timesheet);
    }


    public List<Mission> findAllMissionByEmployeJPQL(int employeId) {
        LOG.info(MessageFormat.format("All Mission By Employe {0}", timesheetRepository.findAllMissionByEmployeJPQL(employeId)));
        return timesheetRepository.findAllMissionByEmployeJPQL(employeId);
    }


    public List<Employe> getAllEmployeByMission(int missionId) {
        LOG.info(MessageFormat.format("All Employe By Mission {0}", timesheetRepository.getAllEmployeByMission(missionId)));
        return timesheetRepository.getAllEmployeByMission(missionId);
    }

}
